var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var minify = require('gulp-clean-css');
var imagemin = require('gulp-imagemin');
var gSequence = require('gulp-sequence');
var autoprefixer = require('gulp-autoprefixer');

var cssPath = 'assets/css';
var jsPath = 'assets/js';
var imagePath = 'resources/images/**/*';

// CSS BUSTER
gulp.task('css:watch', function () {
  var cssFiles = cssPath + '/**/*.scss';
  gulp.watch(cssFiles, ['prod-css']);
});

gulp.task('css', function () {
  return gulp.src(cssPath + '/*.scss')
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(sass({
        outputStyle: 'expanded'
      })
      .on('error', sass.logError)
    )
    .pipe(gulp.dest(cssPath));
});

gulp.task('mini-css', function () {
  return gulp.src([cssPath + '/vendor/*.css', cssPath + '/style.css'])
    .pipe(concat('style.css'))
    .pipe(minify())
    .pipe(gulp.dest('dist'));
})

gulp.task('prod-css', function (callback) {
  gSequence('css', 'mini-css')(callback);
});

// JAVASCRIPT
gulp.task('mini-js', function () {
  return gulp.src(jsPath + '/app.js')
    .pipe(uglify())
    .pipe(gulp.dest('dist'));
});

// IMAGE OPTIMIZE
gulp.task('image', function() {
  return gulp.src(imagePath)
    .pipe(imagemin([
      imagemin.gifsicle({interlaced: true}),
      imagemin.jpegtran({progressive: true}),
      imagemin.optipng({optimizationLevel: 5}),
      imagemin.svgo({
        plugins: [
          {removeViewBox: true},
          {cleanupIDs: false}
        ]
      })], {
        verbose: true
      })
    )
    .pipe(gulp.dest('assets/image/'))
})