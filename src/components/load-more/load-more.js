'use strict';

class loadMore {
    constructor(element) {
        this.$el = $(element);
        this.$target = item.el.attr('href')
            ? item.el.attr('href')
            : item.el.attr('data-load-target');
        this.limit = item.el.attr('data-load-limit')
            ? item.el.attr('data-load-limit')
            : 999;
        this.$children = $(this.$target.children());
    }

    init() {
        this._addHandler();
    }

    _addHandler() {
        this.$el.on('click', e => {
            e.preventDefault();

            this.$target.addClass('load-more_opened');
        });
    }
}

export function initLoadmore() {
    $('.js-load-more').map((index, element) => {
        let loadmore = new loadMore(element);
        loadmore.init();
    });
}
