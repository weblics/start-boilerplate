/**
 * @description
 * node.js core handles the path
 *
 * @type {*|Object}
 * */
const path = require('path');

/**
 * PLUGINS PLACE
 *
 * @type {webpack|exports|module.exports}
 */
const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const DashboardPlugin = require('webpack-dashboard/plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
    .BundleAnalyzerPlugin;

/**
 * Local Config
 */
const files = ['index'];

const registerHtmlPlugin = file => {
    return new HtmlWebPackPlugin({
        template: `./src/${file}.html`,
        filename: `./${file}.html`
    });
};

const htmlPlugins = () => {
    return files.map(file => registerHtmlPlugin(file));
};

module.exports = {
    devtool: 'eval-source-map',
    watch: true,
    devServer: {
        inline: true
    },
    stats: {
        colors: true
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[hash].js',
        chunkFilename: '[name].[hash].js',
        publicPath: '/dist'
    },
    module: {
        rules: [{
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.html$/,
                use: [{
                    loader: 'html-loader',
                    options: {
                        minimize: true
                    }
                }]
            }
        ]
    },
    resolve: {
        modules: ['node_modules'],
        alias: {
            public: path.resolve(__dirname, 'public')
        },
        extensions: ['.js', '.json', 'scss', 'css', '.min.js', '.min.css']
    },
    plugins: [
        new CleanWebpackPlugin('dist', {
            verbose: true
        }),
        new webpack.ProvidePlugin({
            jQuery: 'jquery',
            $: 'jquery',
            jQuery: 'jquery',
            'window.jquery': 'jquery'
        }),
        new BundleAnalyzerPlugin(),
        ...htmlPlugins()
    ],
    optimization: {
        minimize: true,
        runtimeChunk: true,
        splitChunks: {
            chunks: 'async',
            cacheGroups: {
                default: false,
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendor',
                    chunks: 'all',
                    reuseExistingChunk: true
                }
            }
        }
    }
};