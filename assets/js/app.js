// MENEMUKAN PUNCAK DARI SEBUAH ARRAY
function searchPeak(arr){
	var peakCount = 0;
	var peakCurr = 0;
	var curr = 0;

	for(var i = 0; i< arr.length; i++){

		//start
		if(i==0){
			curr = arr[i];
			peakCurr = arr[i];

			//First Peak
			if(curr > arr[i+1])
				peakCount++;

		}else 
		if(i == arr.length -1) {
			curr= arr[i-1];

			//Last Peak
			if(curr < arr[i])
				peakCount++;
		}else{
			curr = arr[i-1];
			
			//Search Peak
			if(arr[i] >= curr && arr[i] > arr[i+1])
				peakCount++;
		}

	}

	return peakCount;
}

// Get Query String
var getQueryString = (function(){
	return{
		init: function(){
			var query = window.location.search.substring(1);
			var vars = query.split('&');
			for (var i = 0; i < vars.length; i++) {
				var pair = vars[i].split('=');
				if (decodeURIComponent(pair[0]) == variable) {
					return decodeURIComponent(pair[1]);
				}
			}
			return "";
		}
	}
}());

// Click to Anchor Section
var AnchorClick = (function(){

	var $html 	= $('html,body'),
		isInit 	= false,
		opts	= {
			time: 200
		};

	var _init = function(){
		
		$("body").on("click", '.ac-link',function(e){
			e.preventDefault();

			var $this 	= $(e.target);
			var target 	= $this.attr('data-ac');
			var topPos	= $(target).offset().top;

			/**
				@TODO : add animation scroll
			*/
			$html.animate({
				scrollTop: topPos
			}, opts.time );

		});
	}

	return {
		init : _init
	}

}());

(function($, window, undefined){

	$(function(){
		alert("mama");
		AnchorClick.init();
	})

}(jQuery, window, undefined));