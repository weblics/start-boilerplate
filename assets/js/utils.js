
// UTILITY
(function (window, $) {

    /**
     * EqualHeight
     * */
    var  u= window.UTIL || {};

    var _equalHeight = function (selector, options) {
        var $this = $(selector),
            $window = $(window),
            defaultOptions = {
                takeHighest: true,
                takeSmallest: false,
                byHeight: true,
                byOuterHeight: false,
                timeoutCalculate: 0,
                disableAt: -1,
                delay: 0
            },
            opts = $.extend({}, defaultOptions, options);

        // internal settings
        var settings = {
                data: [],
                hasInit: false,
                maxHeight: -1,
                minHeight: -1,
                selector: selector
            },
            intervals = null,
            heightIntervals = null;

        // config options
        opts.takeHighest = !opts.takeSmallest;
        opts.byHeight = !opts.byOuterHeight;

        // @global
        var calculate = function () {
            if ($window.width() > opts.disableAt) {
                clearTimeout(heightIntervals);

                if (!settings.hasInit) {
                    settings.hasInit = true;
                }

                settings.data = [];

                $this.css("height", "");

                heightIntervals = setTimeout(function () {
                    setTimeout(function () {
                        $this.each(function () {
                            if (opts.byHeight) {
                                settings.data.push(Math.round($(this).height()));
                            }

                            else if (opts.byOuterHeight) {
                                settings.data.push(Math.round($(this).outerHeight()));
                            }
                        });

                        settings.data.sort(function (a, b) { return b - a; });

                        settings.maxHeight = settings.data[0];
                        settings.minHeight = settings.data[settings.data.length - 1];

                        if (opts.takeHighest) {
                            $this.height(settings.maxHeight);
                        }

                        else if (opts.takeSmallest) {
                            $this.height(settings.minHeight);
                        }
                    }, opts.delay);
                }, opts.timeoutCalculate);
            } else {
                detach();
            }
        };

        // @global
        var refreshElements = function () {
            $this = $(settings.selector);

            // re-calculate after refresh
            calculate();
        };

        // @global
        var getElements = function () {
            return $this;
        };

        // @global
        var detach = function () {
            $this.css("height", "")
        };

        // register
        this.calculate = calculate;
        this.refreshElements = refreshElements;
        this.getElements = getElements;
        this.detach = detach;

        // initialization
        calculate();

        // resize binders
        $window.on("resize", function () {
            clearTimeout(intervals);
            intervals = setTimeout(function () {
                calculate();
            }, 100);
        });
    };

    u.equalHeight = function (selector, options) {
        return new _equalHeight(selector, options);
    };

    window.UTIL = u;

}(window, jQuery));
(function (window, $) {

    /**
     * StickyMenu
     * */
    var u = window.UTIL || {};

    var _sticky = function (selector, options) {
        var opt = $.extend({
            disableAt: -1,
            virtualElement: true,
            onsticked: function () {},
            onunsticked: function () {}
        }, options);

        var $this = $(selector);
        var $body = $(document.body);
        var $window = $(window);
        var $virtualDiv = $(); // optional
        var stickId = "stick-" + Math.random().toString(16).substr(2, 6);

        // internal state
        var scrollTop = -1,
            deviceWidth = -1,
            offset = -1,
            targetHeight = -1,
            timers,
            _class = {
                sticked: "is-sticky"
            },
            _timers = { onstick: null, onunstick: null },
            refresh, recalc, checkMobile;

        // wrapping it
        $this.wrap("<div id=\"" + stickId + "\"></div>");
        $virtualDiv = $this.closest("#" + stickId);

        refresh = this.refresh = function () {
            targetHeight = $this.outerHeight(true);
            offset = Math.abs(Math.round($virtualDiv.offset().top));
            deviceWidth = $window.width();
        };

        checkMobile = this.checkMobile = function () {
            if (deviceWidth <= opt.disableAt) {
                $this.removeClass(_class.sticked);

                if (opt.virtualElement) {
                    $virtualDiv.css("height", "");
                }
            } else {
                recalc();
            }
        };

        recalc = this.recalc = function () {
            scrollTop = $window.scrollTop();

            if (scrollTop > offset) {
                if (!$this.hasClass(_class.sticked)) {
                    clearTimeout(_timers.onstick);
                    _timers.onstick = setTimeout(function () {
                        opt.onsticked();
                    }, 150);
                }

                $this.addClass(_class.sticked);

                if (opt.virtualElement) {
                    $virtualDiv.css("height", targetHeight);
                }
            } else {
                if ($this.hasClass(_class.sticked)) {
                    clearTimeout(_timers.onunstick);
                    _timers.onunstick = setTimeout(function () {
                        opt.onunsticked();
                    }, 150);
                }

                $this.removeClass(_class.sticked);

                if (opt.virtualElement) {
                    $virtualDiv.css("height", "");
                }
            }
        };

        // first init
        this.refresh();
        this.recalc();
        this.checkMobile();

        $window.on("scroll", function () {
            if (deviceWidth <= opt.disableAt) {
                $this.removeClass(_class.sticked);

                if (opt.virtualElement) {
                    $virtualDiv.css("height", "");
                }
            } else {
                recalc();
            }
        });

        $window.on("resize", function () {
            clearTimeout(timers);
            timers = setTimeout(function () {
                refresh();
                checkMobile();
            }, 150);
        });

        return this;
    };

    u.sticky = function (selector, options) {
        return new _sticky(selector, options);
    };

}(window, jQuery));
(function (window, $) {

    /**
     * GetQuery
     * */
    var u = window.UTIL || {};

    var _getQueryString = function(){
        var query = window.location.search.substring(1);
        var vars = query.split('&');
        var variable = "link";
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            if (decodeURIComponent(pair[0]) == variable) {
                return decodeURIComponent(pair[1]);
            }
        }
        return "";
    };

    u.getQueryString = _getQueryString;

}(window, jQuery));
(function (window, $) {

    /**
     * IsMobile
     * */
    var u = window.UTIL || {};

    // CHECK CLIENT BROWSER DEVICE
    var _isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (_isMobile.Android() || _isMobile.BlackBerry() || _isMobile.iOS() || _isMobile.Opera() || _isMobile.Windows());
        }
    };

    u.isMobile = _isMobile;

}(window, jQuery));
(function (window, $) {

    /**
     * Anchor Click
     * */
    var u = window.UTIL || {};

    var _anchorClick = function(selector, options){
        var OPTS = {};

        var $this, topPos, target,
            $html 	= $('html,body'),
            isInit 	= false,
            $opts = $.extend(OPTS, options);

        $html.on("click", selector, function(e){
            e.preventDefault();

            $this 	= $(this);
            target 	= $this.attr('data-ac');
            topPos	= $(target).offset().top;

            $html.animate({
                scrollTop: topPos
            }, opts.time );

        });
    };

    u.anchorClick = _anchorClick;

}(window, jQuery));

